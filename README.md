# Upmind TOTP authentication task

## Setup:
 - Download the repo via `git clone`.
 - Install [Composer](https://getcomposer.org/) if not installed.
 - Run `composer install` within the working directory of the project
 - Configure a web server to handle requests towards `index.php`
 or run the built in server from the php CLI. E.g: `php -S localhost:8080`
 - Navigate to the URL of the app.

## Disclaimer:
This task is not well suited for an API calls as it returns `HTML`, please keep that in mind.