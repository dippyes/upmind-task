<?php
require "vendor/autoload.php";

use OTPHP\TOTP;
use ParagonIE\ConstantTime\Base32;

if (!isset($_POST)
    || !array_key_exists("label", $_POST)
    || !array_key_exists("username", $_POST)
    || strlen($_POST["label"]) === 0
    || strlen($_POST["username"]) === 0
) {
    $response = [
        "header" => "Message",
        "data" => json_encode(["error" => "Missing parameters please provide label and username'"])
    ];
    header("Location:/");
    header(sprintf("%s:%s", $response["header"], $response["data"]));
    die();
}
["username" => $username, "label" => $label] = $_POST;
$totp = TOTP::create(trim(Base32::encodeUpper($username)));
$totp->setLabel(sprintf('%s (%s)', ucwords($username), ucwords($label)));

$google_chart = $totp->getQrCodeUri();

require "ValidationAndQrCode.php";
