<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upmind TOTP Authentication</title>
</head>
<body>
	<?php if(isset($google_chart)) : ?>
	    <img src="<?=$google_chart?>" />
	<?php endif;?>
    <form method="post" action="Validator.php">
        <div>
            <label for="code">OTP Code:</label>
            <input type="text" name="code" id="code" />
        </div>
        <div>
            <label for="username">Username:</label>
            <input type="text" name="username" id="username" required />
        </div>
        <input type="submit" value="Validate OPT code">
        </form>
        <?php if (isset($message) && array_key_exists("error",$message)) : ?>
            <p style="color:red;"><?= $message["error"] ?></p>
        <?php elseif (isset($message) && array_key_exists("success",$message)) : ?>
            <p style="color:green;"><?= $message["success"] ?></p>
        <?php endif; ?>
        </body>
        </html>