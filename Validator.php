<?php
require "vendor/autoload.php";

use OTPHP\TOTP;
use ParagonIE\ConstantTime\Base32;

if (!isset($_POST)
    || !array_key_exists("code", $_POST)
    || !array_key_exists("username", $_POST)
    || strlen($_POST["code"]) === 0
    || strlen($_POST["username"]) === 0
) {
    $response = [
        "header" => "Message",
        "data" => json_encode(["error" => "Missing parameters please provide code and username'"])
    ];
    header(sprintf("%s:%s", $response["header"], $response["data"]));
    require "ValidationAndQrCode.php";
    die();
}
["username" => $username, 'code' => $code] = $_POST;

$totp = TOTP::create(trim(Base32::encodeUpper($username)));

$message = $totp->verify($code) ? ['success' => 'Code/Username match'] : ['error' => "Code/Username mismatch, please try again"];

require "ValidationAndQrCode.php";
